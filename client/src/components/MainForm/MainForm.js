import React from 'react'
import styles from './MainForm.module.css'
import Form from "../Form/Form";
import FormInsert from "../Form-insert/FormInsert";
import ButtonForm from "../ButtonForm/ButtonForm";
import Text from "../Text/Text";


const textSub = "Подписаться можно в один клик прямо сейчас :) ";
const textGet = "Получить расписание можно прямо здесь и сейчас!";

export default class MainForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            formType: 'get',
            date: '21.10.2019',
            city1: 'Самара',
            city2: 'Санкт-Петербург'
        }

    }

    dateChange = (val) => {
        this.setState({date: val })
    };

    city1Change = (val) => {
        this.setState({city1: val });
    };

    city2Change = (val) => {
        this.setState({city2: val });
    };

    getClick = () => {
        this.setState({formType: 'get'});
    };

    subClick = () => {
        this.setState({formType: 'sub'});
    };



    render() {
    const { formType, city1, city2, date } = this.state;
        return(
            <div className={styles.formContent}>
                <Text color="black">{this.state.formType === 'get' ? textGet : textSub}</Text>
                <div className={styles.buttons}>
                    <ButtonForm onClick={this.getClick}>Получить</ButtonForm>
                    <ButtonForm onClick={this.subClick}>Подписаться</ButtonForm>
                </div>
                { formType === 'get' ?
                  <Form
                      date={date}
                      city1={city1}
                      city2={city2}
                      dateChange={this.dateChange}
                      city1Change={this.city1Change}
                      city2Change={this.city2Change}/>
                : <FormInsert
                        date={date}
                        city1={city1}
                        city2={city2}
                        dateChange={this.dateChange}
                        city1Change={this.city1Change}
                        city2Change={this.city2Change} /> }
            </div>
        )
    }
}