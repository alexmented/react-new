import React from 'react'
import { NavLink } from 'react-router-dom'
import styles from "./Menu.module.css"
export default class Menu extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {


        return(
            <menu className={styles.menu}>
                <div className="logo">ScheduleRZD</div>
                <NavLink exact to="/" activeClassName={styles.active}><div>Главная</div></NavLink>
                <NavLink to="/insert" activeClassName={styles.active}><div>Ввод информации</div></NavLink>
                <NavLink to="/schedule" activeClassName={styles.active}><div>Расписание</div></NavLink>
            </menu>
        )
    }
}