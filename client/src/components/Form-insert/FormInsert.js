import React from 'react'
import styles from './FormInsert.module.css'
import Button from "../Button/Button";
import dateParser from "../dateParser"
import {pushingData} from "../../actions";
import {connect} from "react-redux";

class FormInsert extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: ''
        }
    }

    dateChange = (e) => {
        this.props.dateChange(e.target.value);
    };

    city1Change = (e) => {
        this.props.city1Change(e.target.value);
    };

    city2Change = (e) => {
        this.props.city2Change(e.target.value);
    };

    emailChange = (e) => {
        this.setState({email: e.target.value });
    };

    insertData = (e) => {
        e.preventDefault();
        let { city1, city2, date } = this.props;
        this.props.pushDatas(city1, city2, dateParser(date), this.state.email);
        this.setState({email: ''});
    };

    render() {
        return(
            <div className={styles.formInsert}>
                <form  onSubmit={this.insertData}>
                    <select value={ this.props.city1 } onChange={this.city1Change}>
                        <option value="Санкт-Петербург">Санкт-Петербург</option>
                        <option value="Самара">Самара</option>
                    </select>
                    <select value={ this.props.city2 } onChange={this.city2Change}>
                        <option value="Санкт-Петербург">Санкт-Петербург</option>
                        <option value="Самара">Самара</option>
                    </select>
                    <input type="date" placeholder="Дата" value={ this.props.date } onChange={this.dateChange} required/>
                    <input type="email" placeholder="Email" value={ this.state.email } onChange={this.emailChange} required/>
                    <Button type="submit">Подписаться</Button>

                </form>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch =>({
    pushDatas: (city1,city2, date, email)=>dispatch(pushingData(city1,city2, date, email)),
});

export default connect(null, mapDispatchToProps)(FormInsert);