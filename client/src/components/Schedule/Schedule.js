import React from 'react'
import styles from './Schedule.module.css'
import Row from "../Row/Row";

export default class Schedule extends React.Component {
    render() {
        console.log(this.props.schedule);
        if (this.props.schedule.length === 0) {
            return <div className={styles.error}>Что-то пошло не так<div></div></div>
        }
        else if (this.props.schedule !== '') {
            const parseSch = this.props.schedule;

            return (
                <div className={styles.main}>
                    <div className={styles.content}>
                            <div>Поезд номер</div>
                            <div>Город отправления</div>
                            <div>Город прибытия</div>
                            <div>Время отправления</div>
                            <div>Время прибытия</div>
                            <div>Цена люкса</div>
                            <div>Цена СВ</div>
                            <div>Цена плацкарта</div>
                            <div>Цена купе</div>
                            <div>Цена сидячих</div>
                    </div>
                    {parseSch.map(obj => <Row key={obj.id} {...obj}/>)}
                </div>
            )
        }
        else {
            return <div className={styles.error}>Тут вроде должно быть твое расписание<div></div></div>
        }


    }
}