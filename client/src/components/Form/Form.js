import React from 'react'
import styles from './Form.module.css'
import Button from "../Button/Button";
import dateParser from "../dateParser";
import { withRouter } from 'react-router-dom';
import { gettingData } from "../../actions";
import {connect} from "react-redux";

class Form extends React.Component {
    constructor(props) {
        super(props);
    }

    dateChange = (e) => {
        this.props.dateChange(e.target.value);
    };

    city1Change = (e) => {
        this.props.city1Change(e.target.value);
    };

    city2Change = (e) => {
        this.props.city2Change(e.target.value);
    };

    getData = (e) => {
        console.log(1);
       e.preventDefault();
       let { city1, city2, date} = this.props;
       this.props.getDatas(city1, city2, dateParser(date));
       this.props.history.push('/schedule');
    };

    render() {
        return(
            <div className={styles.getStyle}>
                <form  onSubmit={this.getData}>
                    <select value={ this.props.city1 } onChange={this.city1Change}>
                        <option value="Санкт-Петербург">Санкт-Петербург</option>
                        <option value="Самара">Самара</option>
                    </select>
                    <select value={ this.props.city2 } onChange={this.city2Change}>
                        <option value="Санкт-Петербург">Санкт-Петербург</option>
                        <option value="Самара">Самара</option>
                    </select>
                    <input type="date" placeholder="Дата" value={ this.props.date } onChange={this.dateChange}/>
                    <Button type="submit">Получить</Button>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch =>({
    getDatas: (city1,city2, date)=>dispatch(gettingData(city1,city2, date)),
});

export default connect(null, mapDispatchToProps)(withRouter(Form));