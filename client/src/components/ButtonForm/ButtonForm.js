import React from 'react'
import styles from './ButtonForm.module.css'

export default class ButtonForm extends React.Component {

    render() {
        return(
            <button
                onClick={this.props.onClick}
                className={ styles.buttonList }
                type={this.props.type}>{this.props.children}</button>
        )
    }
}