import React from 'react'
import styles from './MainPhoto.module.css'
import Text from "../Text/Text";

export default class MainPhoto extends React.Component {

    render() {
        return(
            <div className={styles.mainPhoto}>
                <Text>Веб-приложение для оповещения о стоимости железнодорожых билетов</Text>
            </div>
        )
    }
}