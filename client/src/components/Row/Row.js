import React from 'react'
import styles from './Row.module.css'

export default class Row extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { departcity, arrivecity, priceluxe, pricecoupe, priceplatz, pricesid, pricesoft,
                name, departtime, arrivetime } = this.props;
        return(
            <div className={styles.row}>
                <div>{name}</div>
                <div>{departcity}</div>
                <div>{arrivecity}</div>
                <div>{departtime}</div>
                <div>{arrivetime}</div>
                <div>{priceluxe}</div>
                <div>{pricesoft}</div>
                <div>{priceplatz}</div>
                <div>{pricecoupe}</div>
                <div>{pricesid}</div>
            </div>
        )
    }
}