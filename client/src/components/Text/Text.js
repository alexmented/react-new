import React from 'react'
import styles from './Text.module.css'

export default class Text extends React.Component {

    render() {
        return(
            <React.Fragment>
            {this.props.color === 'black' ? <div className={styles.textBlack}>{this.props.children}</div>:<div className={styles.textWhite}>{this.props.children}</div>}
            </React.Fragment>
            )
    }
}