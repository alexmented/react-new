export default class Parser {

    async callPostGet(city1, city2, date) {
        const data = { city1: city1, city2: city2, date: date };
        const response = await fetch("http://localhost:3001/testAPI", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });

        const result = await response.json();
        return JSON.stringify(result);
        //return result[0];

    }

    async callPostInsert(city1, city2, date, email) {
        const data = { city1: city1, city2: city2, date: date, email: email };
        const response = await fetch("http://localhost:3001/insertAPI", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });

        const result = await response.json();
        return JSON.stringify(result);
        //return result[0];

    }


    async callPostI() {

        const data = {answer: 4};
        let response = await fetch("http://localhost:3001/testAPI", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });

        let result = await response.json();
        return JSON.stringify(result[0]);
        //return result[0];

    }
}