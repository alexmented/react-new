import React from 'react';
import Menu from "./components/Menu/Menu";
import MainPhoto from "./components/MainPhoto/MainPhoto";
import MainForm from "./components/MainForm/MainForm";
import Schedule from "./components/Schedule/Schedule";
import {
    BrowserRouter as Router, Redirect,
    Route,
} from "react-router-dom";
import {connect} from "react-redux";
import { gettingData } from "./actions";
import { pushingData } from "./actions";


class App extends React.Component {


    componentDidMount() {
        document.title = "ScheduleRZD";
    }

    render() {
        return (
            <div className="App">
                <Router>
                    <Menu/>
                    <Route exact path="/" component={MainPhoto}/>
                    <Route path="/insert" render={() => {
                        return <MainForm />
                    }}/>
                    <Route path="/schedule" render={() => {
                        return <Schedule schedule={this.props.state}/>
                    }}/>
                    <Redirect to="/" />
                </Router>



            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    }
};

const mapDispatchToProps = dispatch =>({
    getData: (city1,city2, date)=>dispatch(gettingData(city1,city2, date)),
    insertData: (city1, city2, date, email)=>dispatch(pushingData(city1, city2, date, email))
});


export default connect(mapStateToProps, mapDispatchToProps
)(App);

